/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <engine.h>
#include <modulemap.h>

#include <QDebug>
#include <QLibrary>
#include <QtTest/QTest>
#include <QObject>

using namespace KStackTrace;

class ModuleMappingTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testCurrentMapping()
    {
        Engine engine;
        const auto map = engine.currentModuleMap();
        QVERIFY(map.get());
        QVERIFY(!map->modules.empty());

        for (const auto &mod : map->modules) {
            qDebug() << (uintptr_t)mod.baseAddress << mod.size << mod.fileOffset << mod.name.c_str();
            QVERIFY(mod.baseAddress > 0ull);
            QVERIFY(mod.size > 0ull);
            QVERIFY(!mod.name.empty());
        }
    }

    void testModuleMonitoring()
    {
        Engine engine;
        const auto oldMap = engine.currentModuleMap();

        QLibrary lib(QCoreApplication::applicationDirPath() + QStringLiteral("/libdummymodule"));
        QVERIFY(lib.load());

        const auto newMap = engine.currentModuleMap();
        QVERIFY(oldMap->modules.size() < newMap->modules.size());

        lib.unload();
        QVERIFY(oldMap->modules.size() == engine.currentModuleMap()->modules.size());
    }

    void benchmarkUncached()
    {
        QBENCHMARK_ONCE {
            Engine engine;
            engine.currentModuleMap();
        }
    }

    void benchmarkCached()
    {
        Engine engine;
        QBENCHMARK {
            engine.currentModuleMap();
        }
    }
};

QTEST_MAIN(ModuleMappingTest)

#include "modulemapping_test.moc"
