/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <engine.h>
#include <trace.h>

#include <QDebug>
#include <QtTest/QTest>
#include <QObject>

using namespace KStackTrace;

class UnwindTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testUnwind()
    {
        Engine engine;
        const auto trace = engine.stackTrace();
        QVERIFY(!trace.frames.empty());
        for (const auto &f : trace.frames) {
            QVERIFY(f);
        }
    }

    void benchmarkUncached()
    {
        QBENCHMARK_ONCE {
            Engine engine;
            engine.stackTrace();
        }
    }

    void benchmarkCached()
    {
        Engine engine;
        engine.stackTrace();
        QBENCHMARK {
            engine.stackTrace();
        }
    }
};

QTEST_APPLESS_MAIN(UnwindTest)

#include "unwind_test.moc"

