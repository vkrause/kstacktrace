/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <engine.h>
#include <frame.h>
#include <modulemap.h>
#include <trace.h>

#include <QDebug>
#include <QtTest/QTest>
#include <QObject>

using namespace KStackTrace;

class BacktraceTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testBacktrace()
    {
        Engine engine;

        auto trace = engine.stackTrace();
        QVERIFY(trace.frames.size() > 0);
        QVERIFY(trace.modules.get());
        QVERIFY(trace.modules->modules.size() > 0);

        for (uint i = 0; i < trace.frames.size(); ++i) {
            const auto resolvedFrame = engine.resolveFrame(trace, i);
            const auto mod = trace.modules->moduleForAddress(trace.frames[i]);
            qDebug() << (uint64_t)trace.frames[i] << resolvedFrame.function.c_str() << resolvedFrame.source.c_str() << resolvedFrame.line << (mod ? mod->name.c_str() : "??");
        }

    }

    void benchmarkResolve()
    {
        Engine engine;
        const auto trace = engine.stackTrace();
        QBENCHMARK {
            for (uint i = 0; i < trace.frames.size(); ++i) {
                auto resolvedFrame = engine.resolveFrame(trace, i);
            }
        }
    }
};

QTEST_APPLESS_MAIN(BacktraceTest)

#include "backtrace_test.moc"
