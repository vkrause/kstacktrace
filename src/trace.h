/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_TRACE_H
#define KSTACKTRACE_TRACE_H

#include "kstacktrace_export.h"

#include "address.h"

#include <memory>
#include <vector>

namespace KStackTrace {

class ModuleMap;

/*! Representation of a unresolved stack trace with raw instruction pointers. */
class Trace
{
public:
    std::shared_ptr<ModuleMap> modules;
    std::vector<Address> frames;
};

}

#endif // KSTACKTRACE_TRACE_H
