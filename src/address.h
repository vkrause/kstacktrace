/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_ADDRESS_H
#define KSTACKTRACE_ADDRESS_H

#include "kstacktrace_export.h"

#include <cstdint>

namespace KStackTrace {

/*! A memory address.
 */
class KSTACKTRACE_EXPORT Address
{
public:
    inline Address(const void *ptr)
    {
        m_addr.ptr = ptr;
    }
    inline Address(uintptr_t p)
    {
        m_addr.data = p;
    }

    inline operator uintptr_t() const
    {
        return m_addr.data;
    }
    inline operator void*() const
    {
        return const_cast<void*>(m_addr.ptr);
    }

    inline bool operator<(Address rhs) const
    {
        return m_addr.data < rhs.m_addr.data;
    }
    inline bool operator<=(Address rhs) const
    {
        return m_addr.data <= rhs.m_addr.data;
    }

private:
    union {
        const void *ptr;
        uintptr_t data;
    } m_addr;
};

inline bool operator<(uintptr_t lhs, Address rhs)
{
    return lhs < static_cast<uintptr_t>(rhs);
}
inline bool operator<(Address lhs, uintptr_t rhs)
{
    return static_cast<uintptr_t>(lhs) < rhs;
}

}

#endif // KSTACKTRACE_ADDRESS_H
