/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_FRAME_H
#define KSTACKTRACE_FRAME_H

#include "kstacktrace_export.h"

#include <memory>
#include <string>
#include <vector>

namespace KStackTrace {

/*! Describes resolved information about a frame. */
class KSTACKTRACE_EXPORT Frame
{
public:
    std::string function;
    std::string source;
    int line = 0;
};

}

#endif // KSTACKTRACE_FRAME_H
