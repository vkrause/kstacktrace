/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_ENGINE_P_H
#define KSTACKTRACE_ENGINE_P_H

#include "config-kstacktrace.h"
#include "modulemonitor.h"

#include <memory>

#if defined(HAVE_LIBDW)
#include <elfutils/libdwfl.h>
#endif

namespace KStackTrace {

class Address;
class ModuleMap;

class EnginePrivate
{
public:
    ~EnginePrivate();

    void initDbgHelpSymHandler();
    void initLibDw();

    Trace libunwindStackTrace();
    Trace execinfoStackTrace();
    Trace dbgHelpStackStrace();

    std::shared_ptr<ModuleMap> dlIterateModules();
    std::shared_ptr<ModuleMap> toolHelp32Modules();

    Frame libdwResolve(Address addr);
    Frame execinfoResolve(Address addr);
    Frame dbgHelpResolve(Address addr);

    void maybeDemangle(std::string &name) const;

    void moduleAdded();
    void moduleRemoved();

    std::shared_ptr<ModuleMap> m_currentModuleMap;
    ModuleMonitor m_moduleMonitor;
private:
    bool m_libunwindInitialized = false;
    void* m_dbgHelpProcesHandle = nullptr;

#ifdef HAVE_LIBDW
    Dwfl* m_dwHandle = nullptr;
    Dwfl_Callbacks m_dwCallbacks;
#endif
};

}

#endif // KSTACKTRACE_ENGINE_P_H

