/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "modulemap.h"
#include "address.h"

#include <algorithm>

using namespace KStackTrace;

void ModuleMap::sort()
{
    std::sort(modules.begin(), modules.end(), [](const Module &lhs, const Module &rhs) {
        return lhs.baseAddress < rhs.baseAddress;
    });
}

const Module* ModuleMap::moduleForAddress(Address addr) const
{
    auto it = std::lower_bound(modules.cbegin(), modules.cend(), addr, [](const Module &lhs, Address addr) {
        return lhs.baseAddress < addr;
    });
    if (it == modules.cend() || it == modules.cbegin())
        return nullptr;
    --it;
    if (it->baseAddress <= addr && addr < it->baseAddress + it->size)
        return &(*it);
    return nullptr;
}
