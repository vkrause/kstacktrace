/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_OS_H
#define KSTACKTRACE_OS_H

#if (defined _WIN32)
#define OS_WINDOWS
#elif (defined __ANDROID__)
#define OS_ANDROID
#elif (defined __linux__)
#define OS_LINUX
#elif (defined __APPLE__) && (defined __MACH__)
#define OS_MACOS
#elif (defined __QNXNTO)
#define OS_QNX
#else
#error "Unsupported operating system!"
#endif

#endif // KSTACKTRACE_OS_H


