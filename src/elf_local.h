/*
    Copyright (C) 2014-2017 Milian Wolff <mail@milianw.de>
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_ELF_LOCAL_H
#define KSTACKTRACE_ELF_LOCAL_H

#include <config-kstacktrace.h>

#include <elf.h>

namespace KStackTrace {

/*! Helpers for using ELF structures in the current (local) process.
 *  This is not useful for anything that needs to deal with different
 *  address sizes.
 */
namespace Elf {
#if ADDRESS_SIZE == 4
typedef Elf32_Dyn Dyn;
typedef Elf32_Rela Rela;
typedef Elf32_Sym Sym;
#define ELF_R_SYM(x) ELF32_R_SYM(x)

#elif ADDRESS_SIZE == 8
typedef Elf64_Dyn Dyn;
typedef Elf64_Rela Rela;
typedef Elf64_Sym Sym;
#define ELF_R_SYM(x) ELF64_R_SYM(x)

#else
#error "Unsupported address size!"
#endif

template <typename T, int64_t AddrTag, int64_t SizeTag>
struct Table
{
    T* table = nullptr;
    uint64_t size = {};

    bool consume(const Dyn* dyn) noexcept
    {
        if (dyn->d_tag == AddrTag) {
            table = reinterpret_cast<T*>(dyn->d_un.d_ptr);
            return true;
        } else if (dyn->d_tag == SizeTag) {
            size = dyn->d_un.d_val;
            return true;
        }
        return false;
    }
};

typedef Table<const char, DT_STRTAB, DT_STRSZ> StringTable;
typedef Table<Rela, DT_JMPREL, DT_PLTRELSZ> JmpRelTable;
typedef Table<Sym, DT_SYMTAB, DT_SYMENT> SymbolTable;

}

}

#endif
