/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_MODULEMONITOR_H
#define KSTACKTRACE_MODULEMONITOR_H

#include "ntdll.h"

#include <functional>

namespace KStackTrace {

/*! Monitors loading and unloading of modules. */
class ModuleMonitor
{
public:
    ModuleMonitor();
    ~ModuleMonitor();

    void setEnabled(bool enable);
    void setMappedCallback(const std::function<void()> &callback);
    void setUnmappedCallback(const std::function<void()> &callback);

    void moduleMapped() const;
    void moduleUnmapped() const;
private:
    bool m_enabled = false;
    std::function<void()> m_mappedCallback;
    std::function<void()> m_unmappedCallback;
    NtDll m_ntdll;
    void* m_cookie = nullptr;
};

}

#endif // KSTACKTRACE_MODULEMONITOR_H
