/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_MODULEMAP_H
#define KSTACKTRACE_MODULEMAP_H

#include "kstacktrace_export.h"
#include "module.h"

#include <vector>

namespace KStackTrace {

class Address;

/*! Represents a snapshot of a process module mapping. */
class KSTACKTRACE_EXPORT ModuleMap
{
public:
    const Module* moduleForAddress(Address addr) const;
    void sort();

    std::vector<Module> modules;
};

}

#endif // KSTACKTRACE_MODULEMAP_H
