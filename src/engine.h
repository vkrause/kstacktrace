/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_ENGINE_H
#define KSTACKTRACE_ENGINE_H

#include "kstacktrace_export.h"

#include <memory>
#include <string>

namespace KStackTrace {

class EnginePrivate;
class Frame;
class ModuleMap;
class Trace;

/*! Generates stack traces, monitors module mappings and resolves frame information. */
class KSTACKTRACE_EXPORT Engine
{
public:
    Engine();
    ~Engine();

    /*! Creates a stack trace from the current instruction pointer. */
    // TODO skip frames, depth limit
    Trace stackTrace();

    /*! Returns the current module mapping of this process. */
    std::shared_ptr<ModuleMap> currentModuleMap();

    /*! Resolve frame to the extend possible. */
    Frame resolveFrame(const Trace &trace, unsigned int index);

private:
    std::unique_ptr<EnginePrivate> d;
};

}

#endif // KSTACKTRACE_ENGINE_H
