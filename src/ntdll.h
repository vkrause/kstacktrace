/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KSTACKTRACE_NTDLL_H
#define KSTACKTRACE_NTDLL_H

#include <os.h>

#ifdef OS_WINDOWS
#include <windows.h>

typedef struct _LSA_UNICODE_STRING {
    USHORT Length;
    USHORT MaximumLength;
    PWSTR  Buffer;
} LSA_UNICODE_STRING, *PLSA_UNICODE_STRING, UNICODE_STRING, *PUNICODE_STRING;

typedef struct _LDR_DLL_LOADED_NOTIFICATION_DATA
{
    ULONG Flags;
    const PUNICODE_STRING FullDllName;
    const PUNICODE_STRING BaseDllName;
    void* DllBase;
    ULONG SizeOfImage;
} LDR_DLL_LOADED_NOTIFICATION_DATA, LDR_DLL_UNLOADED_NOTIFICATION_DATA;

typedef union _LDR_DLL_NOTIFICATION_DATA
{
    LDR_DLL_LOADED_NOTIFICATION_DATA Loaded;
    LDR_DLL_UNLOADED_NOTIFICATION_DATA Unloaded;
} LDR_DLL_NOTIFICATION_DATA;
typedef LDR_DLL_NOTIFICATION_DATA const *PCLDR_DLL_NOTIFICATION_DATA;

typedef VOID (NTAPI *PLDR_DLL_NOTIFICATION_FUNCTION)(ULONG reason, PCLDR_DLL_NOTIFICATION_DATA data, void* context);
#define LDR_DLL_NOTIFICATION_REASON_LOADED UINT32_C(1)
#define LDR_DLL_NOTIFICATION_REASON_UNLOADED UINT32_C(2)
typedef NTSTATUS (NTAPI *PFNLDRREGISTERDLLNOTIFICATION)(ULONG, PLDR_DLL_NOTIFICATION_FUNCTION, void*, void**);
typedef NTSTATUS (NTAPI *PFNLDRUNREGISTERDLLNOTIFICATION)(void*);

namespace KStackTrace {

/*! Access to ntdll.dll content that is not regularly accessible. */
class NtDll
{
public:
    NtDll();
    ~NtDll();

    NTSTATUS ldrRegisterDllNotification(ULONG flags, PLDR_DLL_NOTIFICATION_FUNCTION callback, void *context, void **cookie);
    NTSTATUS ldrUnregisterDllNotification(void *cookie);

private:
    HMODULE m_module = nullptr;
    PFNLDRREGISTERDLLNOTIFICATION m_ldrRegisterDllNotification = nullptr;
    PFNLDRUNREGISTERDLLNOTIFICATION m_ldrUnregisterDllNotification = nullptr;
};

}
#else

namespace KStackTrace {
class NtDll {};
}
#endif

#endif // KSTACKTRACE_NTDLL_H
