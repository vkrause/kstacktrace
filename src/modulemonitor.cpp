/*
    Copyright (C) 2014-2017 Milian Wolff <mail@milianw.de>
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <config-kstacktrace.h>

#include "modulemonitor.h"
#include "os.h"

#include <cstring>
#include <type_traits>

using namespace KStackTrace;

#ifdef HAVE_DL_ITERATE_PHDR
#include "elf_local.h"

#include <link.h>
#include <sys/mman.h>

namespace hooks {

void overwrite_symbols() noexcept;

struct dlopen
{
    static constexpr auto name = "dlopen";
    static constexpr auto original = &::dlopen;

    static void* hook(const char* filename, int flag) noexcept
    {
        auto ret = original(filename, flag);
        overwrite_symbols();
        if (callback)
            callback();
        return ret;
    }

    static std::function<void()> callback;
};
std::function<void()> dlopen::callback;

struct dlclose
{
    static constexpr auto name = "dlclose";
    static constexpr auto original = &::dlclose;

    static int hook(void* handle) noexcept
    {
        auto ret = original(handle);
        if (callback)
            callback();
        return ret;
    }

    static std::function<void()> callback;
};
std::function<void()> dlclose::callback;

template <typename Hook>
bool hook(const char* symname, uintptr_t addr, bool restore)
{
    static_assert(std::is_convertible<decltype(&Hook::hook), decltype(Hook::original)>::value,
                  "hook is not compatible to original function");

    if (strcmp(Hook::name, symname) != 0) {
        return false;
    }

    // try to make the page read/write accessible, which is hackish
    // but apparently required for some shared libraries
    auto page = reinterpret_cast<void*>(addr & ~(0x1000 - 1));
    mprotect(page, 0x1000, PROT_READ | PROT_WRITE);

    // now write to the address
    auto typedAddr = reinterpret_cast<typename std::remove_const<decltype(Hook::original)>::type*>(addr);
    if (restore) {
        // restore the original address on shutdown
        *typedAddr = Hook::original;
    } else {
        // now actually inject our hook
        *typedAddr = &Hook::hook;
    }

    return true;
}

void apply(const char* symname, uintptr_t addr, bool restore)
{
    hook<dlopen>(symname, addr, restore) || hook<dlclose>(symname, addr, restore);
}

void try_overwrite_symbols(const Elf::Dyn* dyn, uintptr_t base, bool restore) noexcept
{
    Elf::SymbolTable symbols;
    Elf::JmpRelTable jmprels;
    Elf::StringTable strings;

    // initialize the elf tables
    for (; dyn->d_tag != DT_NULL; ++dyn) {
        symbols.consume(dyn) || jmprels.consume(dyn) || strings.consume(dyn);
    }

    // find symbols to overwrite
    const auto rela_end = reinterpret_cast<Elf::Rela*>(reinterpret_cast<char*>(jmprels.table) + jmprels.size);
    for (auto rela = jmprels.table; rela < rela_end; rela++) {
        const auto index = ELF_R_SYM(rela->r_info);
        const char* symname = strings.table + symbols.table[index].st_name;
        auto addr = rela->r_offset + base;
        hooks::apply(symname, addr, restore);
    }
}

int iterate_phdrs(dl_phdr_info* info, size_t /*size*/, void* data) noexcept
{
    if (strstr(info->dlpi_name, "KStackTrace.so")) { // TODO based this on this being inside the address range instead
        // prevent infinite recursion: do not overwrite our own symbols
        return 0;
//     } else if (strstr(info->dlpi_name, "/ld-linux")) {
        // prevent strange crashes due to overwriting the free symbol in ld-linux
//         return 0;
    }

    for (auto phdr = info->dlpi_phdr, end = phdr + info->dlpi_phnum; phdr != end; ++phdr) {
        if (phdr->p_type == PT_DYNAMIC)
            try_overwrite_symbols(reinterpret_cast<const Elf::Dyn*>(phdr->p_vaddr + info->dlpi_addr), info->dlpi_addr, data != nullptr);
    }
    return 0;
}

void overwrite_symbols() noexcept
{
    dl_iterate_phdr(&iterate_phdrs, nullptr);
}

void restore_symbols() noexcept
{
    bool restore = true;
    dl_iterate_phdr(&iterate_phdrs, &restore);
}

}
#endif

#ifdef OS_WINDOWS
#include <windows.h>

static void NTAPI dllNotificationCallback(ULONG reason, PCLDR_DLL_NOTIFICATION_DATA data, PVOID context)
{
    switch (reason) {
        case LDR_DLL_NOTIFICATION_REASON_LOADED:
            reinterpret_cast<const ModuleMonitor*>(context)->moduleMapped();
            break;
        case LDR_DLL_NOTIFICATION_REASON_UNLOADED:
            reinterpret_cast<const ModuleMonitor*>(context)->moduleUnmapped();
            break;
    }
}

#endif


ModuleMonitor::ModuleMonitor() = default;

ModuleMonitor::~ModuleMonitor()
{
    setEnabled(false);
}

void ModuleMonitor::setEnabled(bool enable)
{
    if (m_enabled == enable)
        return;
    m_enabled = enable;
#ifdef HAVE_DL_ITERATE_PHDR
    if (m_enabled)
        hooks::overwrite_symbols();
    else
        hooks::restore_symbols();
#endif

#ifdef OS_WINDOWS
    if (m_enabled)
        m_ntdll.ldrRegisterDllNotification(0, dllNotificationCallback, this, &m_cookie);
    else
        m_ntdll.ldrUnregisterDllNotification(m_cookie);
#endif
}

void ModuleMonitor::setMappedCallback(const std::function<void ()>& callback)
{
#ifdef HAVE_DL_ITERATE_PHDR
    hooks::dlopen::callback = callback;
#endif
    m_mappedCallback = callback;
}

void ModuleMonitor::setUnmappedCallback(const std::function<void ()>& callback)
{
#ifdef HAVE_DL_ITERATE_PHDR
    hooks::dlclose::callback = callback;
#endif
    m_unmappedCallback = callback;
}

void ModuleMonitor::moduleMapped() const
{
    if (m_mappedCallback)
        m_mappedCallback();
}

void ModuleMonitor::moduleUnmapped() const
{
    if (m_unmappedCallback)
        m_unmappedCallback();
}
