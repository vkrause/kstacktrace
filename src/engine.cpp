/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config-kstacktrace.h"

#include "engine.h"
#include "engine_p.h"
#include "architecture.h"
#include "frame.h"
#include "modulemap.h"
#include "trace.h"

#include <cassert>
#include <cstring>

#if defined(HAVE_LIBUNWIND)
#define UNW_LOCAL_ONLY
#include <libunwind.h>
#include <unistd.h>
#endif

#if defined(HAVE_EXECINFO_BACKTRACE) || defined(HAVE_EXECINFO_BACKTRACE_SYMBOLS)
#include <execinfo.h>
#endif

#if defined(HAVE_DBGHELP)
#include <windows.h>
#include <dbghelp.h>
#endif
#ifdef HAVE_TOOLHELP32
#include <windows.h>
#include <tlhelp32.h>
#endif

#ifdef HAVE_DL_ITERATE_PHDR
#include <link.h>
#endif

#ifdef HAVE_CXA_DEMANGLE
#include <cxxabi.h>
#endif

using namespace KStackTrace;

EnginePrivate::~EnginePrivate()
{
#ifdef HAVE_DBGHELP
    if (m_dbgHelpProcesHandle)
        SymCleanup(m_dbgHelpProcesHandle);
#endif

#ifdef HAVE_LIBDW
    if (m_dwHandle)
        dwfl_end(m_dwHandle);
#endif
}

void EnginePrivate::initDbgHelpSymHandler()
{
#ifdef HAVE_DBGHELP
    if (!m_dbgHelpProcesHandle) {
        m_dbgHelpProcesHandle = GetCurrentProcess();
        SymInitialize(m_dbgHelpProcesHandle, nullptr, true);

        auto symOptions = SymGetOptions();
        symOptions |= SYMOPT_LOAD_LINES;
        symOptions |= SYMOPT_FAIL_CRITICAL_ERRORS;
        symOptions |= SYMOPT_UNDNAME;
        SymSetOptions(symOptions);
    }
#endif
}

void EnginePrivate::initLibDw()
{
#ifdef HAVE_LIBDW
    if (m_dwHandle)
        return;

    m_dwCallbacks.find_elf = &dwfl_linux_proc_find_elf;
    m_dwCallbacks.find_debuginfo = &dwfl_standard_find_debuginfo;
    m_dwCallbacks.debuginfo_path = nullptr;

    m_dwHandle = dwfl_begin(&m_dwCallbacks);

    // ### temporary
    dwfl_report_begin(m_dwHandle);
    auto r = dwfl_linux_proc_report(m_dwHandle, getpid());
    dwfl_report_end(m_dwHandle, nullptr, nullptr);
#endif
}

Trace EnginePrivate::libunwindStackTrace()
{
    Trace t;
#ifdef HAVE_LIBUNWIND
    if (!m_libunwindInitialized) {
        m_libunwindInitialized = true;
        if (unw_set_caching_policy(unw_local_addr_space, UNW_CACHE_PER_THREAD))
            fprintf(stderr, "WARNING: Failed to enable per-thread libunwind caching.\n");
//         if (unw_set_cache_size(unw_local_addr_space, 1024))
//             fprintf(stderr, "WARNING: Failed to set libunwind cache size.\n");
    }

    void* trace[256];
    const auto n = unw_backtrace(trace, sizeof(trace));

    t.frames.reserve(n);
    std::copy(trace, trace + n, std::back_inserter(t.frames));
#endif
    return t;
}

Trace EnginePrivate::execinfoStackTrace()
{
    Trace t;
#ifdef HAVE_EXECINFO_BACKTRACE
    void* trace[256];
    const auto n = backtrace(trace, sizeof(trace));

    t.frames.reserve(n);
    std::copy(trace, trace + n, std::back_inserter(t.frames));
#endif
    return t;
}

Trace EnginePrivate::dbgHelpStackStrace()
{
    initDbgHelpSymHandler();

    Trace t;
#ifdef HAVE_DBGHELP
    CONTEXT context;
    memset(&context, 0, sizeof(CONTEXT));
    context.ContextFlags = CONTEXT_FULL;
    RtlCaptureContext(&context);

    STACKFRAME64 frame;
    memset(&frame, 0, sizeof(STACKFRAME64));

#if defined(ARCH_X64)
    constexpr auto imgType = IMAGE_FILE_MACHINE_AMD64;
    frame.AddrPC.Offset = context.Rip;
    frame.AddrPC.Mode = AddrModeFlat;
    frame.AddrFrame.Offset = context.Rsp;
    frame.AddrFrame.Mode = AddrModeFlat;
    frame.AddrStack.Offset = context.Rsp;
    frame.AddrStack.Mode = AddrModeFlat;
#elif defined(ARCH_X86)
    constexpr auto imgType = IMAGE_FILE_MACHINE_I386;
    frame.AddrPC.Offset = context.Eip;
    frame.AddrPC.Mode = AddrModeFlat;
    frame.AddrFrame.Offset = context.Ebp;
    frame.AddrFrame.Mode = AddrModeFlat;
    frame.AddrStack.Offset = context.Esp;
    frame.AddrStack.Mode = AddrModeFlat;
#endif

    int count = 32; // TODO
    while (StackWalk64(imgType, GetCurrentProcess(), GetCurrentThread(), &frame, &context, nullptr, nullptr, nullptr, nullptr) && count--) {
        t.frames.push_back(frame.AddrPC.Offset);
    }

#endif
    return t;
}

#ifdef HAVE_DL_ITERATE_PHDR
static int dlIteratePhdrCallback(struct dl_phdr_info *info, size_t /*size*/, void *data)
{
    auto m = reinterpret_cast<ModuleMap*>(data);

    for (int i = 0; i < info->dlpi_phnum; i++) {
        const auto& phdr = info->dlpi_phdr[i];
        if (phdr.p_type == PT_LOAD && phdr.p_flags & PF_X) {
            Module mod;
            mod.baseAddress = phdr.p_vaddr + info->dlpi_addr;
            mod.size = phdr.p_memsz;
            mod.fileOffset = phdr.p_offset;
            mod.name = info->dlpi_name;
            if (mod.name.empty())
                mod.name = "<self>"; // TODO
            m->modules.push_back(std::move(mod));
        }
    }

    return 0;
}
#endif

std::shared_ptr<ModuleMap> EnginePrivate::dlIterateModules()
{
    std::shared_ptr<ModuleMap> m;
#ifdef HAVE_DL_ITERATE_PHDR
    m = std::make_shared<ModuleMap>();
    dl_iterate_phdr(dlIteratePhdrCallback, m.get());
#endif
    return m;
}

std::shared_ptr<ModuleMap> EnginePrivate::toolHelp32Modules()
{
    std::shared_ptr<ModuleMap> map;
#ifdef HAVE_TOOLHELP32
    MODULEENTRY32 me;
    me.dwSize = sizeof(MODULEENTRY32);
    const auto snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetCurrentProcessId());
    if (snapshot == INVALID_HANDLE_VALUE)
        return map;

    map = std::make_shared<ModuleMap>();
    for (bool hasNext = Module32First(snapshot, &me); hasNext; hasNext = Module32Next(snapshot, &me)) {
        Module m;
        m.baseAddress = reinterpret_cast<uint64_t>(me.modBaseAddr);
        m.size = me.modBaseSize;
        m.name = me.szModule;
        map->modules.push_back(std::move(m));
    }

    CloseHandle(snapshot);
#endif
    return map;
}

Frame EnginePrivate::libdwResolve(Address addr)
{
    initLibDw();

    Frame f;
#ifdef HAVE_LIBDW
    if (!m_dwHandle)
        return f;

    // module and symbol name
    auto mod = dwfl_addrmodule(m_dwHandle, addr);
    if (!mod)
        return f;
    f.function = dwfl_module_addrname(mod, addr);

    // source location
    Dwarf_Addr mod_bias = 0;
    Dwarf_Die* cudie = dwfl_module_addrdie(mod, addr, &mod_bias);
    if (!cudie)
        return f;
    Dwarf_Line* srcloc = dwarf_getsrc_die(cudie, addr - mod_bias);
    if (srcloc) {
            f.source = dwarf_linesrc(srcloc, 0, 0);
            dwarf_lineno(srcloc, &f.line);
    }
#endif

    return f;
}

Frame EnginePrivate::execinfoResolve(Address addr)
{
    Frame f;
#ifdef HAVE_EXECINFO_BACKTRACE_SYMBOLS
    void* ip = addr;
    auto buffer = backtrace_symbols(&ip, 1);

    auto begin = strstr(buffer[0], "(");
    if (begin) {
        ++begin;
        auto end = strstr(begin, "+");
        if (end)
            f.function = std::string(begin, end - begin);
    }
    free(buffer);
#endif
    return f;
}

Frame EnginePrivate::dbgHelpResolve(Address addr)
{
    initDbgHelpSymHandler();

    Frame f;
#ifdef HAVE_DBGHELP
    char buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
    PSYMBOL_INFO pSymbol = (PSYMBOL_INFO)buffer;
    pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    pSymbol->MaxNameLen = MAX_SYM_NAME;
    if (SymFromAddr(GetCurrentProcess(), addr, nullptr, pSymbol)) {
        f.function = pSymbol->Name;
    } else {
        DWORD error = GetLastError();
//         fprintf(stderr, "SymFromAddr returned error : %lld %d\n", (uint64_t)addr, error);
    }

    IMAGEHLP_LINE64 line;
    memset(&line, 0, sizeof(IMAGEHLP_LINE64));
    line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);

    DWORD displacement;
    if (SymGetLineFromAddr64(GetCurrentProcess(), addr, &displacement, &line)) {
          f.line = line.LineNumber;
          f.source = line.FileName;
    } else {
        DWORD error = GetLastError();
//         fprintf(stderr, "SymGetLineFromAddr64 returned error : %lld %d\n", (uint64_t)addr, error);
    }
#endif
    return f;
}


void EnginePrivate::maybeDemangle(std::string& name) const
{
#ifdef HAVE_CXA_DEMANGLE
    if (name.size() < 2)
        return;
    if (name.compare(0, 2, "_Z") == 0) { // Linux
        int status = -1;
        auto demangled = abi::__cxa_demangle(name.c_str(), nullptr, nullptr, &status);
        if (status == 0 && demangled) {
            name = demangled;
            free(demangled);
        }
    } else if (name.compare(0, 1, "Z") == 0) { // mingw, for some reason the leading '_' is missing there...
        name.insert(0, 1, '_');
        int status = -1;
        auto demangled = abi::__cxa_demangle(name.c_str(), nullptr, nullptr, &status);
        if (status == 0 && demangled) {
            name = demangled;
            free(demangled);
        } else {
            name.erase(0, 1);
        }
    }
#endif
}

void EnginePrivate::moduleAdded()
{
    printf("moduleAdded\n");
    m_currentModuleMap.reset();
}

void EnginePrivate::moduleRemoved()
{
    printf("moduleRemoved\n");
    m_currentModuleMap.reset();
}


Engine::Engine()
    : d(new EnginePrivate)
{
    d->m_moduleMonitor.setMappedCallback([this]{ d->moduleAdded(); });
    d->m_moduleMonitor.setUnmappedCallback([this]{ d->moduleRemoved(); });
    d->m_moduleMonitor.setEnabled(true); // TODO delay
}

Engine::~Engine() = default;

Trace Engine::stackTrace()
{
    Trace trace = {
#if defined(HAVE_LIBUNWIND)
        d->libunwindStackTrace()
#elif defined(HAVE_EXECINFO_BACKTRACE)
        execinfoUnwind()
#elif defined(HAVE_DBGHELP)
        d->dbgHelpStackStrace()
#endif
    };

    trace.modules = currentModuleMap();
    return trace;
}

std::shared_ptr<ModuleMap> Engine::currentModuleMap()
{
    if (!d->m_currentModuleMap) {
        d->m_currentModuleMap = {
#ifdef HAVE_DL_ITERATE_PHDR
            d->dlIterateModules()
#elif defined(HAVE_TOOLHELP32)
            d->toolHelp32Modules()
#endif
        };
        d->m_currentModuleMap->sort();
    }
    return d->m_currentModuleMap; // TODO reload when we are not able to monitor
}

Frame Engine::resolveFrame(const Trace &trace, unsigned int index)
{
    assert(index < trace.frames.size());

    Frame frame = {
#if defined(HAVE_LIBDW)
        d->libdwResolve(trace.frames[index])
#elif defined(HAVE_EXECINFO_BACKTRACE_SYMBOLS)
        d->execinfoResolve(trace.frames[index])
#elif defined HAVE_DBGHELP
        d->dbgHelpResolve(trace.frames[index])
#endif
    };

    d->maybeDemangle(frame.function);

    return frame;
}
