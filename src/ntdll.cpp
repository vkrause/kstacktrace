/*
    Copyright (C) 2017 Volker Krause <vkrause@kde.org>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ntdll.h"

#include <stdio.h>

using namespace KStackTrace;

#ifdef OS_WINDOWS
NtDll::NtDll()
{
    m_module = LoadLibraryA("ntdll.dll");
    if (!m_module) {
        fprintf(stderr, "Failed to load ntdll.dll: %d\n", GetLastError());
    }

    m_ldrRegisterDllNotification = reinterpret_cast<PFNLDRREGISTERDLLNOTIFICATION>(GetProcAddress(m_module, "LdrRegisterDllNotification"));
    m_ldrUnregisterDllNotification = reinterpret_cast<PFNLDRUNREGISTERDLLNOTIFICATION>(GetProcAddress(m_module, "LdrUnregisterDllNotification"));
    if (!m_ldrRegisterDllNotification || !m_ldrUnregisterDllNotification)
        fprintf(stderr, "Failed to load proc address.\n");
}

NtDll::~NtDll()
{
    if (m_module)
        FreeLibrary(m_module);
}

NTSTATUS NtDll::ldrRegisterDllNotification(ULONG flags, PLDR_DLL_NOTIFICATION_FUNCTION callback, void *context, void **cookie)
{
    if (!m_ldrRegisterDllNotification)
        return 0;
    return m_ldrRegisterDllNotification(flags, callback, context, cookie);
}

NTSTATUS NtDll::ldrUnregisterDllNotification(void *cookie)
{
    if (!m_ldrUnregisterDllNotification)
        return 0;
    return m_ldrUnregisterDllNotification(cookie);
}

#endif
